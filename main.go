package main

import (
	"github.com/Ullaakut/nmap"
	"log"
	"fmt"
	"net"
	"strings"
	"strconv"
	"time"
	"bytes"
	"io"
)

func getScanerIP() net.IP {
    conn, err := net.Dial("udp", "8.8.8.8:80")
    if err != nil {
        log.Fatal(err)
    }
    defer conn.Close()

    localAddr := conn.LocalAddr().(*net.UDPAddr)

    return localAddr.IP
}

func makeTargetsArray(userIP net.IP)[]string{
	var targets []string
	stringer := userIP.String()
	defaut := strings.Split(stringer, ".")
	front := defaut[0]+"."+defaut[1]+"."+defaut[2]
	for i := 0; i < 255; i++{
		s := strconv.Itoa(i)
		x := front + "." + s
		targets = append(targets,x)
	}
	return targets

}

func main(){
	userIP := getScanerIP()
	targets := makeTargetsArray(userIP)
	s, err := nmap.NewScanner(
		nmap.WithTargets(targets...),
		//nmap.WithFastMode(),
		nmap.WithSYNDiscovery(),
		//nmap.WithUDPDiscovery(),
		//nmap.WithCustomDNSServers("8.8.8.8", "8.8.4.4"),
		//nmap.WithTCPScanFlags(nmap.FlagACK, nmap.FlagNULL, nmap.FlagRST),
	)
	if err != nil {
		log.Fatalf("unable to create nmap scanner: %v", err)
	}
	fmt.Printf("Scanning the network...\n\n")
	scanResult, err := s.Run()
	if err != nil {
		log.Fatalf("nmap encountered an error: %v", err)
	}
	fmt.Printf("Found %v Hosts Up\n",len(scanResult.Hosts))
	time.Sleep(1 * time.Second)
	for _, host := range scanResult.Hosts{
		dialTest(host)
	}
	
}

func dialTest(host nmap.Host){
	fmt.Printf("\nHost-IP: %v\nHost Ports: %v\n",host.Addresses,host.Ports)
	for _, port := range host.Ports{
		fmt.Printf("%v\n",port.ID)
		conn, err := net.Dial("tcp", host.Addresses[0].Addr+":"+strconv.Itoa(int(port.ID)))
		if err != nil {
			fmt.Printf("Dial Error: %v\n", err)
			continue
		}
		conn.Write([]byte("/"))
		var buf bytes.Buffer
		go func(){
			io.Copy(&buf, conn)
		}()
		time.Sleep(3 * time.Second)
		conn.Close()
		fmt.Printf("Respost for Port %v : %v\n", port, buf)
		}
}